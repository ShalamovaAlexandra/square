package ru.omsu.imit;

import org.junit.Test;

import static org.junit.Assert.*;

public class SquareSolveTest {

    private double[] array2 = {-1,-2};
    private double[] array={-1};
    private double[] arr = {};
    @Test
    public void twoRootsLessZero()throws IllegalArgumentException{
        assertArrayEquals(array2,new SquareSolve(1,3,2).rootSearch(),0.0001);
    }
    @Test
    public void oneRoot()throws IllegalArgumentException{
        assertArrayEquals(array,new SquareSolve(1,2,1).rootSearch(),0.0001);
    }
    @Test
    public void noRoots()throws IllegalArgumentException{
        assertArrayEquals(arr,new SquareSolve(0,0,1).rootSearch(),0.0001);
    }
    @Test
    public void discLessZero()throws IllegalArgumentException{
        assertArrayEquals(arr,new SquareSolve(1,2,3).rootSearch(),0.0001);
    }
    @Test(expected = IllegalArgumentException.class)
    public void infinityRoots()throws IllegalArgumentException {
        assertArrayEquals(arr,new SquareSolve(0,0,0).rootSearch(),0.0001);
        fail();
    }
}