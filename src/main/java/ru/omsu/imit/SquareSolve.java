package ru.omsu.imit;

public class SquareSolve {
    private double a, b, c;
    public SquareSolve(){}
    public  SquareSolve(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public SquareSolve(SquareSolve scr) {
        a = scr.a;
        b = scr.b;
        c = scr.c;
    }

    public double[] rootSearch() throws IllegalArgumentException {
        double[] rootArray;
        if (a == 0) {
            if (b == 0) {
                if (c == 0) {
                    throw new IllegalArgumentException("Infinity amount of roots");
                } else return new double[0];
            } else {
                rootArray = new double[1];
                rootArray[0] = (-c) / b;
            }
        } else {
            double disc = b * b - 4 * a * c;
            if (disc == 0) {
                rootArray = new double[1];
                rootArray[0] = (-b) / (2 * a);
            } else if (disc > 0) {
                rootArray = new double[2];
                rootArray[0] = ((-b) + Math.sqrt(disc)) / (2 * a);
                rootArray[1] = ((-b) - Math.sqrt(disc)) / (2 * a);
            } else {
                return new double[0];
            }
        }
        return rootArray;
    }
}
